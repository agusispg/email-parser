const rawEmailBody = `"External email. Open with Caution"

Created Time:2021-01-13T15:38:13+0000
Ad Name: HR-V - AR - FB - Leads - Jan'21 - # - Remarketing
Platform: fb
First Name: Malik
Last Name: Alhamady
Email: malik_444mm@hotmail.com
Phone Number: +96551683268
What is your current vehicle? كرولا
`
const parsedEmailData = [];

const emailBody = rawEmailBody.replace(/\n/g, '|').replace(/\s\s+/g, "").replace(/\t+/g, "")

const createdTimePattern = /(?<=Ad Name:)(.*?)(?=\|)/
const createdTime = emailBody.match(createdTimePattern);
if (createdTime) {
    parsedEmailData.push({'createdTime':createdTime[0]});
}

const adNamePattern = /(?<=Ad Name:)(.*?)(?=\|)/
const adName = emailBody.match(adNamePattern);
if (adName) {
    parsedEmailData.push({'adName':adName[0]});
}

const platformPattern = /(?<=Platform:)(.*?)(?=\|)/
const platform = emailBody.match(platformPattern);
if (platform) {
    parsedEmailData.push({'platform':platform[0]});
}

const firstNamePattern = /(?<=First Name:)(.*?)(?=\|)/
const firstName = emailBody.match(firstNamePattern);
if (firstName) {
    parsedEmailData.push({'firstName':firstName[0]});
}

const lastNamePattern = /(?<=Last Name:)(.*?)(?=\|)/
const lastName = emailBody.match(lastNamePattern);
if (lastName) {
    parsedEmailData.push({'lastName':lastName[0]});
}

const emailPattern = /(?<=Email:)(.*?)(?=\|)/
const email = emailBody.match(emailPattern);
if (email) {
    parsedEmailData.push({'email':email[0]});
}

const phoneNoPattern = /(?<=Phone Number:)(.*?)(?=\|)/
const phoneNo = emailBody.match(phoneNoPattern);
if (phoneNo) {
    parsedEmailData.push({'phoneNo':phoneNo[0]});
}

const currentVehiclePattern = /(?<=What is your current vehicle\?)(.*?)(?=\|)/
const currentVehicle = emailBody.match(currentVehiclePattern);
if (currentVehicle) {
    parsedEmailData.push({'currentVehicle':currentVehicle[0]});
}


console.log(parsedEmailData)
