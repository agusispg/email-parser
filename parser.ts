const rawEmailBody = `Wip No:	
23974

POS Company:	
SO13

Customer Name:           	
Mr Erwin Ma Amparo Eseo

Phone No:	
97891532

Target Magic:	
459254

Appointment date:	
15th December 2020

Appointment time :	
07:00-07:15

First line of Service:	
Service and Maintenance

Text info:	
service maintenance

Comments:	
N/A

Pickup details:	
N/A

Drop details:	
N/A

Modified (Y/N):	
No
`

const parsedEmailData = [];

const emailBody = rawEmailBody.replace(/\n/g, '-').replace(/\s\s+/g, "").replace(/\t+/g, "")

const wipPattern = /(?<=Wip No:\-)(.*?)(?=-)/
const wip = emailBody.match(wipPattern);
if (wip) {
    parsedEmailData.push({'wip':wip[0]});
}

const POSCompanyPattern = /(?<=POS Company:\-)(.*?)(?=-)/
const POSCompany = emailBody.match(POSCompanyPattern);
if (POSCompany) {
    parsedEmailData.push({'company':POSCompany[0]});
}
const customerNamePattern = /(?<=Customer Name:\-)(.*?)(?=-)/
const customerName = emailBody.match(customerNamePattern);
if (customerName) {
    parsedEmailData.push({'customerName':customerName[0]});
}
const phoneNoPattern = /(?<=Phone No:\-)(.*?)(?=-)/
const phoneNo = emailBody.match(phoneNoPattern);
if (phoneNo) {
    parsedEmailData.push({'phoneNo':phoneNo[0]});
}
const targetMagicPattern = /(?<=Target Magic:\-)(.*?)(?=-)/
const targetMagic = emailBody.match(targetMagicPattern);
if (targetMagic) {
    parsedEmailData.push({'targetMagic':targetMagic[0]});
}
const appointmentDatePattern = /(?<=Appointment date:\-)(.*?)(?=-)/
const appointmentDate = emailBody.match(appointmentDatePattern);
if (appointmentDate) {
    parsedEmailData.push({'appointmentDate':appointmentDate[0]});
}
const appointmentTimePattern = /(?<=Appointment time :\-)(.*?)(?=-)/
const appointmentTime = emailBody.match(appointmentTimePattern);
if(appointmentTime) {
    parsedEmailData.push({'appointmentTime':appointmentTime[0]});
}
const firstLineOfServicePattern = /(?<=First line of Service:\-)(.*?)(?=-)/
const firstLineOfService = emailBody.match(firstLineOfServicePattern);
if (firstLineOfService) {
    parsedEmailData.push({'firstLineOfService':firstLineOfService[0]});
}
const textInfoPattern = /(?<=Text info:\-)(.*?)(?=-)/
const textInfo = emailBody.match(textInfoPattern);
if (textInfo) {
    parsedEmailData.push({'textInfo':textInfo[0]});
}
const commentsPattern = /(?<=Comments:\-)(.*?)(?=-)/
const comments = emailBody.match(commentsPattern);
if (comments) {
    parsedEmailData.push({'comments':comments[0]});
}
const pickupDetailsPattern = /(?<=Pickup details:\-)(.*?)(?=-)/
const pickupDetails = emailBody.match(pickupDetailsPattern);
if (pickupDetails) {
    parsedEmailData.push({'pickupDetails':pickupDetails[0]});
}
const dropDetailsPattern = /(?<=Drop details:\-)(.*?)(?=-)/
const dropDetails = emailBody.match(dropDetailsPattern);
if (dropDetails) {
    parsedEmailData.push({'dropDetails':dropDetails[0]});
}
const modifiedPattern = /(?<=Modified (Y\/N):\-)(.*?)(?=-)/
const modified = emailBody.match(modifiedPattern);
if (modified) {
    parsedEmailData.push({'modified':modified[0]});
}

